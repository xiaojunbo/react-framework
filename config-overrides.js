// const path = require('path');
// const paths = require('react-scripts/config/paths');
// const FilemanagerWebpackPlugin = require('filemanager-webpack-plugin');
const TinypngWebpackPlugin = require('tinypng-webpack-plugin');
const { override, addBabelPlugin } = require('customize-cra');
const env = process.env;

// 图片压缩KEY
const tinypngKey = [
  env.npm_package_tinypngKey_0,
  env.npm_package_tinypngKey_1,
  env.npm_package_tinypngKey_2,
  env.npm_package_tinypngKey_3,
  env.npm_package_tinypngKey_4
];

// NPM RUN BABEL时为TRUE，用于下面配置图片压缩
const babel = process.env.npm_lifecycle_event === 'babel';

// 是否清除调试信息
const clean = true;
module.exports = override(addBabelPlugin(['@babel/plugin-proposal-decorators', { legacy: true }]), (config) => {
  // ES6语法加强编译
  config.entry = [require.resolve('@babel/polyfill'), config.entry];

  // 开发环境配置
  // if (env === 'development') {
  if (config.mode === 'development') {
    // 设置map文件在开发环境中可以追踪到SCSS源文件
    config.devtool = 'eval-source-map';

    // 开启样式源文件追踪
    // let rules = config.module.rules[1];
    // console.log(rules.oneOf[3]);
    // rules.oneOf[3].use[1].options.sourceMap = true;
    // rules.oneOf[3].use[2].options.sourceMap = true;
    // rules.oneOf[4].use[1].options.sourceMap = true;
    // rules.oneOf[4].use[2].options.sourceMap = true;
    // rules.oneOf[5].use[1].options.sourceMap = true;
    // rules.oneOf[5].use[2].options.sourceMap = true;
    // rules.oneOf[5].use[3].options.sourceMap = true;
    // rules.oneOf[6].use[1].options.sourceMap = true;
    // rules.oneOf[6].use[2].options.sourceMap = true;
    // rules.oneOf[6].use[3].options.sourceMap = true;
  } else {
    // 生产环境配置
    // 生产环境不要MAP文件
    config.devtool = 'none';

    // paths.appBuild = path.join(path.dirname(paths.appBuild), 'dist');
    // config.output.path = path.join(path.dirname(config.output.path), 'dist');

    // 修改输出文件夹
    Object.assign(config.output, {
      //   filename: 'js/[name].[contenthash:5].min.js',
      //   chunkFilename: 'js/[name].[contenthash:5].chunk.js',
      publicPath: './'
    });

    // 设置输出的JS文件只生成一个
    config.optimization.splitChunks = {};
    config.optimization.runtimeChunk = false;

    // 修改CSS文件名
    // console.log(config.plugins[5].options);
    // config.plugins[5].options.filename = 'css/[name].[contenthash:5].min.css';
    // config.plugins[5].options.chunkFilename = 'css/[name].[contenthash:5].chunk.css';
    // config.plugins[5].options = {
    //   filename: 'css/[name].[contenthash:5].min.css',
    //   chunkFilename: 'css/[name].[contenthash:5].chunk.css'
    // };

    // CSS压缩不输出MAP文件
    config.optimization.minimizer[1].options.cssProcessorOptions.map = false;

    // 图片输出路经
    // console.log(JSON.stringify(config.module.rules[1]));
    // config.module.rules[1].oneOf[0].options.name = 'img/[name].[hash:5].[ext]';
    // config.module.rules[1].oneOf[7].options.name = 'plugin/[name].[hash:5].[ext]';

    // scss中的图片路径修改
    // config.module.rules[1].oneOf[5].use[0].options.publicPath = '../';

    // config.module.rules[1].oneOf.push(config.module.rules[2].oneOf.shift());

    // manifest.json输入路径
    // config.plugins[6].opts.publicPath = './';
    // config.plugins[6].opts.fileName = 'plugin/asset-manifest.json';

    // 删除GenerateSW
    // console.log(config.plugins);
    // config.plugins.splice(8, 1);

    // 图片加哈希值
    // config.module.rules[1].use[0].options.fallback.options.name = 'img/[name].[hash:5].[ext]';

    // 图片转成BASE6的阀值
    // config.module.rules[1].use[0].options.limit = 50000;

    // 设置不要生成 xxx.js.LICENSE.txt
    config['optimization']['minimizer'][0].options.extractComments = false;

    // 打包到线上环境使用，压缩图片和清理调试信息
    if (babel) {
      // 清除调试信息
      if (clean) {
        let compress = config['optimization']['minimizer'][0].options.terserOptions.compress;
        compress.drop_console = true;
        compress.drop_debugger = true;
      }

      config.plugins.push.apply(config.plugins, [
        //压缩图片
        new TinypngWebpackPlugin({
          key: tinypngKey
        })
      ]);
    }

    // 打包生ZIP压缩包
    // config.plugins.push.apply(config.plugins, [
    //   //自动打包成zip文件
    //   new FilemanagerWebpackPlugin({
    //     events: {
    //       onEnd: [
    //         {
    //           copy: [
    //             {
    //               source: path.resolve(__dirname, './build'),
    //               destination: path.resolve(__dirname, './tmp_for_zip/build')
    //             }
    //           ]
    //         },
    //         {
    //           archive: [
    //             {
    //               source: path.resolve(__dirname, './tmp_for_zip'),
    //               destination: path.resolve(__dirname, './build.zip')
    //             }
    //           ]
    //         },
    //         {
    //           delete: [path.resolve(__dirname, './tmp_for_zip')]
    //         }
    //       ]
    //     }
    //   })
    // ]);
  }

  return config;
});
