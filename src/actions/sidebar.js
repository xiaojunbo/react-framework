// 默认数据
export default {
  // 主题
  menus: [
    {
      name: 'page'
    },
    {
      name: 'layout'
    },
    {
      name: 'components'
    },
    {
      name: 'effects'
    },
    {
      name: 'template'
    }
  ]
};

// 设置主题
export function SET_MENU() {
  return 1;
}
