// 样式
import React, { Component } from 'react';
import { LayerById } from '../../lib/react-layer';
import Bbb from '../bbb';
class Aaa extends Component {
  constructor(props) {
    super(props);
    // console.dir(this);
    this.state = {
      content: ['这是一条内容0'],
      timer: null,
      aaa: ''
    };
  }

  static on = {
    show() {
      console.log('static inner show');
    },
    hide() {
      console.log('static inner hide');
    },
    dark() {
      console.log('static inner dark');
    }
  };

  componentDidMount() {
    this.timer = setTimeout(() => {
      this.setState({
        aaa: `<p>${1}</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p><p>1111111</p>`
      });
    }, 5000);
  }

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  render() {
    const { content } = this.state;
    return (
      <div className="layer">
        <div
          onClick={() => {
            LayerById('root').show({
              key: 'bbb',
              content: <Bbb />
            });
          }}
        >
          打开弹层1
        </div>
        <br />
        <br />
        <button
          onClick={() => {
            let _content = content;
            _content.push('这是一条内容' + _content.length);
            this.setState({ content: _content });
          }}
        >
          添加一条记录
        </button>
        <br />
        <br />
        <div stop="touchmove" style={{ height: '3rem', overflow: 'auto' }}>
          <div>1111</div>
          {this.state.aaa}
          <div>1111</div>
          {content.map((item, index) => {
            return <div key={item + '_' + index}>{item}</div>;
          })}
        </div>
        <br />
        <br />
        <br />
        <div
          onClick={() => {
            LayerById('root').hide({
              key: 'aaa',
              on: {
                show: () => {
                  console.log('hide show onclick');
                },
                hide: () => {
                  console.log('hide hide onclick');
                }
              }
            });
          }}
        >
          关闭
        </div>
      </div>
    );
  }
}
export default Aaa;
