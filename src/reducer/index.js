import { combineReducers } from 'redux';
import actions from '../actions';

const _reducers = {};
for (let modName in actions) {
  const { reducers, store } = actions[modName] || {};
  if (reducers) {
    _reducers[modName] = (state = store, { type, data }) => {
      const { name } = type;
      const reducer = reducers[name] || reducers[type];
      if (reducer) {
        return { ...state, ...reducer(state, data) };
      } else {
        return state;
      }
    };
  }
}

export default combineReducers(_reducers);
